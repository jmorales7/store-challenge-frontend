import axios from 'axios';
// initial state
const state = () => ({
  isCreating: false,
  token: localStorage.getItem('token') || null,
  error: null,
  authorName: 'Juan Pablo Morales'
});

// getters
const getters = {
  isCreating: state => state.isCreating,
  getToken: state => state.token,
  getError: state => state.error,
  isAuthenticated(state) {
    return state.token !== null
  }
}

// actions
const actions = {
  async signUp({ commit }, user) {
    commit('setUserIsCreating', true);
    await axios.post(`${process.env.VUE_APP_API_URL}/users`, user)
      .then(res => {
        localStorage.setItem('token',res.data.jwt)
        commit('setUserToken', res.data.jwt);
        commit('setUserIsCreating', false);
        commit('setUserError', null);
      }).catch(err => {
        console.log('error', err.response.data.msg);
        commit('setUserIsCreating', false);
        commit('setUserError', err.response.data.msg);

      });
  },
  async signIn({ commit }, user) {
    commit('setUserIsCreating', true);
    await axios.post(`${process.env.VUE_APP_API_URL}/sign-in`, user)
      .then(res => {
        localStorage.setItem('token',res.data.jwt)
        commit('setUserError', null);
        commit('setUserToken', res.data.jwt);
        commit('setUserIsCreating', false);
      }).catch(err => {
        commit('setUserIsCreating', false);
        commit('setUserError', err.response.data.msg);

      });
  },
  async signOut({commit}) {
    localStorage.removeItem('token')
    commit('setUserToken',null)
  }
}

// mutations
const mutations = {
  setUserIsCreating(state, isCreating) {
    state.isCreating = isCreating
  },
  setUserToken(state, token) {
    state.token = token
  },
  setUserError(state,error) {
    state.error = error
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}