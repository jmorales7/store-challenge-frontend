import axios from 'axios';

// initial state
const state = () => ({
  products: [],
  productsPaginatedData: null,
  product: null,
  isLoading: false,
  isCreating: false,
  createdData: null,
  isUpdating: false,
  updatedData: null,
  isDeleting: false,
  deletedData: null,
  error: null
})

// getters
const getters = {
  productList: state => state.products,
  productsPaginatedData: state => state.productsPaginatedData,
  product: state => state.product,
  isLoading: state => state.isLoading,
  isCreating: state => state.isCreating,
  isUpdating: state => state.isUpdating,
  createdData: state => state.createdData,
  updatedData: state => state.updatedData,
  getError: state => state.error,
  isDeleting: state => state.isDeleting,
  deletedData: state => state.deletedData
};

// actions
const actions = {
  async fetchAllProducts({ commit }, query = null) {
    let page = 1;
    let limit = 10;
    let name = null;
    let sku = null;
    let from = null;
    let to = null;
    let sellerId = null;

    if(query !== null){
      page = query?.page || 1;
      name = query?.name || null;
      sku = query?.sku || null;
      from = query?.from || null;
      to = query?.to || null;
      sellerId = query?.sellerId || null;

    }
    commit('setProductIsLoading', true);
    let url = `${process.env.VUE_APP_API_URL}/products?`;
    
    if (name !== null) {
      url += `name=${name}&`;
    }
    if (sku !== null) {
      url += `sku=${sku}&`;
    }
    if (from !== null) {
      url += `from=${from}&`;
    }
    if (to !== null) {
      url += `to=${to}&`;
    }
    if (sellerId !== null) {
      url += `search=${sellerId}&`;
    }
    await axios.get(url,{
      params: {
        page: page.toString(),
        limit: limit.toString()
      }})
      .then(res => {
        const products = res.data.results;
        commit('setProducts', products);
        const pagination = {
          total: res.data.total,  // total number of elements or items
          per_page: res.data.limit, // items per page
          current_page: res.data.page, // current page (it will be automatically updated when users clicks on some page number).
          total_pages: res.data.pages // total pages in record
        }
        res.data.pagination = pagination;
        commit('setProductsPaginated', res.data);
        commit('setProductIsLoading', false);
      }).catch(err => {
        console.log('error', err);
        commit('setProductIsLoading', false);
      });
  },

  async fetchDetailProduct({ commit }, id) {
    commit('setProductIsLoading', true);
    commit('setErrorProducts', null);
    await axios.get(`${process.env.VUE_APP_API_URL}/products/${id}`)
      .then(res => {
        commit('setProductDetail', res.data);
        commit('setProductIsLoading', false);
      }).catch(err => {
        console.log('error', err);
        let message = ''
        if(!err.response.data.msg) {
          err.response.data.map(e =>{
            message = message + ' ' + e.message
          })
        } else {    
          message = JSON.stringify(err.response.data.msg)
        }
        commit('setErrorProducts', message);
        commit('setProductIsLoading', false);
      });
  },

  async storeProduct({ commit }, product) {
    commit('setProductIsCreating', true);
    commit('setErrorProducts', null);
    await axios.post(`${process.env.VUE_APP_API_URL}/products`, product)
      .then(res => {
        
        commit('saveNewProducts', res.data);
        commit('setProductIsCreating', false);
      }).catch(err => {
        console.log('error', err.response);
        let message = ''
        if(!err.response.data.msg) {
          err.response.data.map(e =>{
            message = message + ' ' + e.message
          })
        } else {    
          message = JSON.stringify(err.response.data.msg)
        }
        commit('setErrorProducts', message);
        commit('setProductIsCreating', false);
      });
  },

  async updateProduct({ commit }, product) {
    commit('setProductIsUpdating', true);
    commit('setErrorProducts', null);
    const id = product.id
    delete product.id
    await axios.put(`${process.env.VUE_APP_API_URL}/products/${id}`, product)
      .then(res => {
        commit('saveUpdatedProduct', res.data);
        commit('setProductIsUpdating', false);
      }).catch(err => {
        console.log('error', err.response);
        let message = ''
        if(!err.response.data.msg) {
          err.response.data.map(e =>{
            message = message + ' ' + e.message
          })
        } else {    
          message = JSON.stringify(err.response.data.msg)
        }
        commit('setErrorProducts', message);
        commit('setProductIsUpdating', false);
      });
  },


  async deleteProduct({ commit }, id) {
    commit('setProductIsDeleting', true);
    await axios.delete(`${process.env.VUE_APP_API_URL}/products/${id}`)
      .then(res => {
        commit('setDeleteProduct', res.data.id);
        commit('setProductIsDeleting', false);
      }).catch(err => {
        console.log('error', err);
        commit('setProductIsDeleting', false);
      });
  },

  updateProductInput({ commit }, e) {
    commit('setProductDetailInput', e);
  }
}

// mutations
const mutations = {
  setProducts: (state, products) => {
    state.products = products
  },

  setProductsPaginated: (state, productsPaginatedData) => {
    state.productsPaginatedData = productsPaginatedData
  },

  setProductDetail: (state, product) => {
    state.product = product
  },

  setDeleteProduct: (state, id) => {
    state.productsPaginatedData.data.filter(x => x.id !== id);
  },

  setProductDetailInput: (state, e) => {
    let product = state.product;
    product[e.target.name] = e.target.value;
    state.product = product
  },

  saveNewProducts: (state, product) => {
    state.products.unshift(product)
    state.createdData = product;
  },

  saveUpdatedProduct: (state, product) => {
    state.products.unshift(product)
    state.updatedData = product;
  },

  setProductIsLoading(state, isLoading) {
    state.isLoading = isLoading
  },

  setProductIsCreating(state, isCreating) {
    state.isCreating = isCreating
  },

  setProductIsUpdating(state, isUpdating) {
    state.isUpdating = isUpdating
  },

  setProductIsDeleting(state, isDeleting) {
    state.isDeleting = isDeleting
  },
  setErrorProducts(state,error) {
    state.error = error
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}