import axios from "axios";
import store from "../store/Store"

// Add a request interceptor
axios.interceptors.request.use(
    config => {
        const token = store.getters['auth/getToken'];
        if (token) {
            config.headers['Authorization'] = `JWT ${token}`;
        }
        return config;
    },
    error => {
        Promise.reject(error)
    });

//Add a response interceptor
axios.interceptors.response.use((response) => {
    return response
}, function(error) {
    return Promise.reject(error);
});