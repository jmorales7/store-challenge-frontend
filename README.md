# Challenge - Vue
## Juan Pablo Morales Rodriguez

- ✨Magic ✨

## Features

- vue js
- axios
- eslint
- vee-validate
- docker

## Implementation

> For this implementation, the vue js framework was used and a whole CRUD was made to consume the backend of the challenge.

## Commands
Install the dependencies and devDependencies and start the server to run local.

```sh
npm install
npm run dev
```